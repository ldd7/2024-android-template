package com.android.basiclib.base.mvi

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow

import kotlinx.coroutines.launch

/**
 * @author Newki
 *
 * 基于BaseViewModel实现，用于实现 MVI 模式的使用
 * <p>
 * 需要指定 Intent（事件） 与 State（状态） 加 Effect（UI效果）三种泛型类型
 */
abstract class BaseEISViewModel<E : IUIEffect, I : IUiIntent, S : IUiState> : BaseISViewModel<I, S>() {

    //一次性事件，无需更新
    private val _effectFlow = MutableSharedFlow<E>()
    val uiEffectFlow: SharedFlow<E> by lazy { _effectFlow.asSharedFlow() }

    //两种方式发射，在协程外用viewModelScope发射
    protected fun sendEffect(builder: suspend () -> E?) = viewModelScope.launch {
        builder()?.let { _effectFlow.emit(it) }
    }

    //两种方式发射,suspend 协程中直接发射
    protected suspend fun sendEffect(effect: E) = _effectFlow.emit(effect)

}