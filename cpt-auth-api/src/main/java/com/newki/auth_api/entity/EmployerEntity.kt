package com.newki.auth_api.entity

data class EmployerEntity(val name: String, val age: Int, val city: String)
