package com.newki.auth_api.router

import com.alibaba.android.arouter.launcher.ARouter

//ARouter 的组件服务提供
object AuthServiceProvider {

    var authService: IAuthService? = ARouter.getInstance().navigation(IAuthService::class.java)

}