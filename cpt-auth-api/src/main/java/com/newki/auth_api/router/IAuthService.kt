package com.newki.auth_api.router

import com.alibaba.android.arouter.facade.template.IProvider

interface IAuthService : IProvider {
    fun isUserLogin(): Boolean
    fun doUserLogin()
}
